\babel@toc {french}{}
\contentsline {chapter}{Préambule}{v}{chapter*.1}%
\contentsline {chapter}{Authentification}{vii}{chapter*.2}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1} problèmatique }{1}{subsection.1.1.1}%
\contentsline {section}{\numberline {1.2}Tâche}{2}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Première objectif}{2}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Second objectif}{2}{subsection.1.2.2}%
\contentsline {section}{\numberline {1.3}Evaluation}{3}{section.1.3}%
\contentsline {section}{\numberline {1.4}Aperçu}{3}{section.1.4}%
\contentsline {section}{\numberline {1.5}Remerciements}{3}{section.1.5}%
\contentsline {section}{\numberline {1.6} code }{3}{section.1.6}%
\contentsline {chapter}{\numberline {2}Architecture}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1} fonctionnement global }{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}frontend}{6}{section.2.2}%
\contentsline {section}{\numberline {2.3}serveur d’authentification}{6}{section.2.3}%
\contentsline {section}{\numberline {2.4}orchestrateur}{7}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1} map d'identification }{7}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2} websocket }{8}{subsection.2.4.2}%
\contentsline {section}{\numberline {2.5}traefik}{8}{section.2.5}%
\contentsline {chapter}{\numberline {3}Implementation}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1} traefik et redirection}{9}{section.3.1}%
\contentsline {section}{\numberline {3.2}frontend}{10}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1} barre de navigation }{10}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2} authentification et enregistement }{11}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}descripteur de pipeline}{12}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4} sélection d’une description et exécution du pipeline}{13}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}orchestrateur}{15}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}description}{16}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}pipeline}{16}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3} serveur d'authentification}{17}{subsection.3.3.3}%
\contentsline {chapter}{\numberline {4}descripteur de pipeline}{19}{chapter.4}%
\contentsline {section}{\numberline {4.1}général}{19}{section.4.1}%
\contentsline {section}{\numberline {4.2}Variables}{20}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}utilisation des variables}{20}{subsection.4.2.1}%
\contentsline {section}{\numberline {4.3}tâche}{21}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}artifacts et dependencies}{21}{subsection.4.3.1}%
\contentsline {section}{\numberline {4.4}tâche utilisable}{22}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}clone}{22}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}logs}{23}{subsection.4.4.2}%
\contentsline {subsection}{\numberline {4.4.3} analyse }{24}{subsection.4.4.3}%
\contentsline {chapter}{\numberline {5}Evaluation}{27}{chapter.5}%
\contentsline {subsection}{\numberline {5.0.1}Représentation}{27}{subsection.5.0.1}%
\contentsline {subsection}{\numberline {5.0.2}exemple de Questionnaire}{27}{subsection.5.0.2}%
\contentsline {section}{\numberline {5.1}métrique}{28}{section.5.1}%
\contentsline {section}{\numberline {5.2} automatisation de la récolte de donnée }{28}{section.5.2}%
\contentsline {chapter}{\numberline {6} amélioration et évolution }{31}{chapter.6}%
\contentsline {section}{\numberline {6.1} base de donnée }{31}{section.6.1}%
\contentsline {section}{\numberline {6.2} DNS et surcharge du serveur }{32}{section.6.2}%
\contentsline {section}{\numberline {6.3} nombre de commits en absolue }{32}{section.6.3}%
\contentsline {section}{\numberline {6.4} évolution }{32}{section.6.4}%
\contentsline {chapter}{\numberline {7}conclusion}{35}{chapter.7}%
\contentsline {section}{\numberline {7.1}difficulté rencontrer}{35}{section.7.1}%
\contentsline {section}{\numberline {7.2} contributions }{36}{section.7.2}%
\contentsline {section}{\numberline {7.3}résumer des connaissances apprises}{36}{section.7.3}%
\contentsline {section}{\numberline {7.4}recommandation}{36}{section.7.4}%
\contentsline {section}{\numberline {7.5} futur possible de ce projet }{36}{section.7.5}%
\contentsline {chapter}{Bibliographie}{39}{section.7.5}%
