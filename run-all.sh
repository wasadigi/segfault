#!/bin/bash

mvn clean install -f ./microservices/auth-service/spring-server/pom.xml
cp ./microservices/auth-service/spring-server/target/swagger-spring-1.0.0.jar ./microservices/auth-service/images/alpine-java/

docker build -t git ./microservices/Orchestrator/express-api/dockerfiles/git-image
docker build -t readwrite ./microservices/Orchestrator/express-api/dockerfiles/readAndWrite
docker build -t node ./microservices/Orchestrator/express-api/dockerfiles/node-image

cd ./topology
docker-compose up --build