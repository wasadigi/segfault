import axios from "axios";
import authHeader from "../services/auth-header";
const API_URL = "/orchestrator/";

class PipelineService {
  /**
   * envois et sauvegarde une description de pipeline pour l'utilisateur courrant
   * @param {json} JSONdescription une description json d'un pipeline
   */
  createDescription(JSONdescription) {
    return axios
      .post(`${API_URL}pipeline/description`, JSONdescription, {
        headers: authHeader()
      })
      .then((response) => response.data);
  }
  getDescriptions() {
    return axios
      .get(`${API_URL}pipeline/descriptions`, {
        headers: authHeader()
      })
      .then((response) => response.data);
  }

  getDescription(description) {
    return axios
      .get(`${API_URL}pipeline/description/${JSON.stringify(description)}`, {
        headers: authHeader()
      })
      .then((response) => response.data);
  }
  create(description) {
    return axios
      .post(`${API_URL}pipeline`, description, {
        headers: authHeader()
      })
      .then((response) => response.data);
  }

  getPipelines() {
    return axios
      .get(`${API_URL}pipeline`, {
        headers: authHeader()
      })
      .then((response) => response.data);
  }
  getPipeline(pipeline) {
    return axios
      .get(`${API_URL}pipeline/${pipeline.name}_${pipeline.version}`, {
        headers: authHeader()
      })
      .then((response) => response.data);
  }

  getProgress(pipeline) {
    return axios
      .get(
        `${API_URL}pipeline/progress/${pipeline.name}-${pipeline.version}-${pipeline.date}`,
        {
          headers: authHeader()
        }
      )
      .then((response) => response.data);
  }
  getResults(pipeline) {
    return axios
      .get(
        `${API_URL}pipeline/results/${pipeline.name}-${pipeline.version}-${pipeline.date}`,
        {
          headers: authHeader()
        }
      )
      .then((response) => response.data);
  }
}

export default new PipelineService();
