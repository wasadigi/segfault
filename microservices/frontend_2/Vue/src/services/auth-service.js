import axios from "axios";

//based on https://bezkoder.com/jwt-vue-vuex-authentication/

const API_URL = "/api-auth";

class AuthService {
  login(user) {
    return axios
      .post(API_URL + "/connection", {
        //todo changer ca en signin
        email: user.email,
        password: user.password
      })
      .then(response => {
        if (response.data.token) {
          user.token = response.data.token;
          localStorage.setItem("user", JSON.stringify(user));
          localStorage.setItem("token", JSON.stringify(response.data.token));
        }
        return user;
      });
  }

  logout() {
    localStorage.removeItem("user");
    //todo call logout in API for token revocation
  }

  register(user) {
    //todo: may better to have signup
    return axios.post(API_URL + "/users", user.toJSON());
  }
}

export default new AuthService();
