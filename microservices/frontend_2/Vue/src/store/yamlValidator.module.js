import YamlValidatorService from "../services/yamlValidator.service";

export const yamlValidator = {
  namespaced: true,

  actions: {
    verifie({ commit }, text) {
      YamlValidatorService.verifie(text).then(
        success => {
          commit("yaml is well formed", success);
          return Promise.resolve(success);
        },
        error => {
          commit("yaml is not formed well");
          return Promise.reject(error);
        }
      );
    }
  }
};
