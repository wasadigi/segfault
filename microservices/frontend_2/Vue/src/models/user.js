export default class User {
  constructor(firstName, lastName, email, password) {
    this.email = email;
    this.firstName = firstName;
    this.isAdmin = false;
    this.lastName = lastName;
    this.password = password;
    this.token = "";
  }

  toJSON() {
    return {
      email: this.email,
      firstName: this.firstName,
      isAdmin: this.isAdmin,
      lastName: this.lastName,
      password: this.password
    };
  }
}
