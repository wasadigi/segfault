import java.io.*;
class Save implements Serializable {
    private int number;
    private String name;

    public Save(int number, String name) {
        this.number = number;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Save{" +
                "number=" + number +
                ", name='" + name + '\'' +
                '}';
    }
}
public class reader {

    public static void main(String[] args) {
        Save save = null;
        try {
            // Reading the object from a file
            FileInputStream file = new FileInputStream("./volume/save_test.ser");
            ObjectInputStream in = new ObjectInputStream(file);

            save = (Save) in.readObject();
            System.out.println(save);
        } catch (FileNotFoundException e) {
            System.out.println("file not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error initializing stream");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("ClassNotFoundException ");
            e.printStackTrace();
        }


    }


}
