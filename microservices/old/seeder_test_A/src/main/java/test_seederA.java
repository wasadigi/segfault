import java.io.*;
class Save implements Serializable {
    private int number;
    private String name;

    public Save(int number, String name) {
        this.number = number;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Save{" +
                "number=" + number +
                ", name='" + name + '\'' +
                '}';
    }
}
public class test_seederA {

    public static void main(String[] args) {
        Save save = new Save(3, "trois");
        try {
            FileOutputStream file = new FileOutputStream(new File("./volume/save_test.ser"));
            ObjectOutputStream o = new ObjectOutputStream(file);
            o.writeObject(save);
            System.out.println("j'envois :" + save);
            o.flush();
            o.close();
            file.close();
        } catch (FileNotFoundException e) {
            System.out.println("file not found!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error initializing stream");
            e.printStackTrace();
        }

    }

}
