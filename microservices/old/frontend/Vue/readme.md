# But 

créer une page web avec vue.js et bulma pour accéder à nos fonctionnalité 

### installation 

```bash
## installation du Commande Ligne Interface vue (global) ; necessite npm
npm i -g @vue/cli
## creation du projet vue 
Vue create frontend
```

### run en local (hotreaload)

taper la commande où ce situe le package.json

```sh
npm run serve 
```

## build pour la prod 

```bash
npm run build
```

### renommer un projet vue

- dans package.json
- dans package.lock 
- dans index.html

### todo (semaine 3)

- faire du lazy loading pour l'i18n
- corriger le problème avec vueX pour l'user
- ~~mettre le build dans un docker et l'intergrer à la topologie traefic~~ 1 journée entière pour ca.
- corriger le problème des sous-components avec le rerendering
- se renseigner sur les test e2e et les test unitaire avec vue

## todo after : 

- spring and vue good way : https://developer.okta.com/blog/2018/11/20/build-crud-spring-and-vue