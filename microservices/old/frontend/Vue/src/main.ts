import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import VueI18n from "vue-i18n";
import messages from "./lang/messages";
import dateTimeFormats from "./lang/dateTimeFormats";
import numberFormats from "./lang/numberFormats";
//import bulma librairy
require("./assets/main.scss");

//use i18n
Vue.use(VueI18n);

Vue.config.productionTip = false;

//setup i18n
let locale = navigator.language;
const i18n = new VueI18n({
  fallbackLocale: "en-US",
  locale: locale,
  messages,
  dateTimeFormats,
  numberFormats
});

//create the main vue instance
new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
