import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from "../actions/user";
import Vue from "vue";
import { AUTH_LOGOUT } from "../actions/auth";
const axios = require("axios").default;

interface State {
  status: string;
  profile: {
    name: string;
  };
}

const state = { status: "", profile: { name: "User name" } };

const getters = {
  getProfile: (state: State) => {
    // eslint-disable-next-line no-console
    console.log("getters user getProfile :" + JSON.stringify(state));
    return state.profile;
  },
  isProfileLoaded: (state: State) => !!state.profile.name
};

const actions = {
  [USER_REQUEST]: ({ commit, dispatch }: any, user: string) => {
    // eslint-disable-next-line no-console
    console.log("user - action " + user);
    commit(USER_REQUEST);
    //par la suite on aura besoin de ça pour recupérer les infos de l'user

    //   axios({
    //     url: "http://192.168.99.100/api-auth/users",
    //     method: "GET"
    //   })
    //     .then((resp: JSON) => {
    //       commit(USER_SUCCESS, resp);
    //     })
    //     .catch(() => {
    //       commit(USER_ERROR);
    //       // if resp is unauthorized, logout, to
    //       dispatch(AUTH_LOGOUT);
    //     });
    commit(USER_SUCCESS, user);
  }
};

const mutations = {
  [USER_REQUEST]: (state: State) => {
    // eslint-disable-next-line no-console
    console.log("USER REQUEST" + JSON.stringify(state));
    state.status = "loading";
  },
  [USER_SUCCESS]: (state: State, resp: string) => {
    // eslint-disable-next-line no-console
    console.log("USER_SUCESS " + resp + "state = " + JSON.stringify(state));
    state.status = "success";
    state.profile = {
      name: resp
    };
    // eslint-disable-next-line no-console
    console.log("USER SUCESS" + JSON.stringify(state));
    Vue.set(state, "profile", { name: resp });
  },
  [USER_ERROR]: (state: State) => {
    state.status = "error";
  },
  [AUTH_LOGOUT]: (state: State) => {
    //todo: state.profile = {}
    state.profile = { name: "" };
  }
};

export default { state, getters, actions, mutations };
