import {
  AUTH_REQUEST,
  AUTH_ERROR,
  AUTH_SUCCESS,
  AUTH_LOGOUT
} from "../actions/auth";
import { USER_REQUEST } from "../actions/user";
const axios = require("axios").default;

interface State {
  token: string;
  status: string;
  profile: {
    name: string;
  };
  hasLoadedOnce: boolean;
}

const state = {
  token: localStorage.getItem("user-token") || "",
  status: "",
  hasLoadedOnce: false
};
const getters = {
  isAuthenticated: (state: State) => !!state.token,
  authStatus: (state: State) => state.status
};

const actions = {
  [AUTH_REQUEST]: (
    { commit, dispatch }: any,
    user: { username: string; password: string }
  ) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST);
      // eslint-disable-next-line no-console
      console.log("auth - " + user.username + " " + user.password);
      axios({
        method: "post",
        //TODO harcoder url
        url: "http://192.168.99.100/api-auth/connection",
        //TODO : access control origin *
        headers: {
          "Access-Control-Allow-Origin": "*"
        },
        data: {
          email: user.username,
          password: user.password
        }
      })
        .then((resp: { token: string }) => {
          // eslint-disable-next-line no-console
          console.log("response " + JSON.stringify(resp));
          localStorage.setItem("user-token", resp.token);
          // Here set the header of your ajax library to the token value.
          // example with axios
          axios.defaults.headers.common["Authorization"] = resp.token;
          commit(AUTH_SUCCESS, resp);
          dispatch(USER_REQUEST, user.username);
          resolve(resp);
        })
        .catch((err: string) => {
          commit(AUTH_ERROR, err);
          localStorage.removeItem("user-token");
          reject(err);
        });
    });
  },
  [AUTH_LOGOUT]: ({ commit }: any) => {
    return new Promise(resolve => {
      commit(AUTH_LOGOUT);
      localStorage.removeItem("user-token");
      resolve();
    });
  }
};
const mutations = {
  [AUTH_REQUEST]: (state: { status: string }) => {
    state.status = "loading";
  },
  [AUTH_SUCCESS]: (
    state: { status: string; token: string; hasLoadedOnce: boolean },
    resp: { token: string }
  ) => {
    state.status = "success";
    state.token = resp.token;
    state.hasLoadedOnce = true;
  },
  [AUTH_ERROR]: (state: { status: string; hasLoadedOnce: boolean }) => {
    state.status = "error";
    state.hasLoadedOnce = true;
  },
  [AUTH_LOGOUT]: (state: { token: string }) => {
    state.token = "";
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
