export default {
  "en-US": {
    language: "no languages | Language | Languages",
    Signup: "Sign up",
    Login: "Log in",
    Home: "Home",
    about: "about",
    localLanguage: "English-US",
    // pour exemple
    hello: "hello world",
    world: "{} world",
    apple: "no apples | one apple | {count} apples"
  },
  "fr-FR": {
    language: "aucune langue | Langue | Langues",
    Signup: "s'enregistrer",
    Login: "Connexion",
    Home: "Acceuil",
    about: "À propos de nous",
    localLanguage: "Français",
    // exemple
    hello: "Bonjour le monde",
    world: "{} le monde",
    apple: "aucune pomme | une pomme | {count} pommes"
  }
};
