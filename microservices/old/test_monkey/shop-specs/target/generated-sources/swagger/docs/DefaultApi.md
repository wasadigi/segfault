# DefaultApi

All URIs are relative to *http://localhost:8081/api-shop*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addProduct**](DefaultApi.md#addProduct) | **POST** /products | 
[**deleteOrder**](DefaultApi.md#deleteOrder) | **DELETE** /shop | 
[**deleteProduct**](DefaultApi.md#deleteProduct) | **DELETE** /products/{productId} | 
[**getOrders**](DefaultApi.md#getOrders) | **GET** /shop | 
[**getProduct**](DefaultApi.md#getProduct) | **GET** /products/{productId} | 
[**getProducts**](DefaultApi.md#getProducts) | **GET** /products | 
[**makeOrder**](DefaultApi.md#makeOrder) | **POST** /shop | 


<a name="addProduct"></a>
# **addProduct**
> addProduct(authorization, product)



add a product

### Example
```java
// Import classes:
import io.avalia.shop.ApiClient;
import io.avalia.shop.ApiException;
import io.avalia.shop.Configuration;
import io.avalia.shop.models.*;
import io.avalia.shop.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/api-shop");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String authorization = "authorization_example"; // String | 
    ProductDTO product = new ProductDTO(); // ProductDTO | 
    try {
      apiInstance.addProduct(authorization, product);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#addProduct");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  |
 **product** | [**ProductDTO**](ProductDTO.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | product added successfully |  -  |
**403** | forbidden |  -  |

<a name="deleteOrder"></a>
# **deleteOrder**
> deleteOrder(authorization, orderId)



delete an order

### Example
```java
// Import classes:
import io.avalia.shop.ApiClient;
import io.avalia.shop.ApiException;
import io.avalia.shop.Configuration;
import io.avalia.shop.models.*;
import io.avalia.shop.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/api-shop");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String authorization = "authorization_example"; // String | 
    Integer orderId = 56; // Integer | 
    try {
      apiInstance.deleteOrder(authorization, orderId);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#deleteOrder");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  |
 **orderId** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | order deleted successfully |  -  |
**403** | forbidden |  -  |
**404** | order not found |  -  |

<a name="deleteProduct"></a>
# **deleteProduct**
> deleteProduct(productId, authorization)



delete a product

### Example
```java
// Import classes:
import io.avalia.shop.ApiClient;
import io.avalia.shop.ApiException;
import io.avalia.shop.Configuration;
import io.avalia.shop.models.*;
import io.avalia.shop.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/api-shop");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    Integer productId = 56; // Integer | 
    String authorization = "authorization_example"; // String | 
    try {
      apiInstance.deleteProduct(productId, authorization);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#deleteProduct");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Integer**|  |
 **authorization** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | product deleted successfully |  -  |
**403** | forbidden |  -  |
**404** | product not found |  -  |

<a name="getOrders"></a>
# **getOrders**
> List&lt;Order&gt; getOrders(authorization, userId)



get the user&#39;s orders

### Example
```java
// Import classes:
import io.avalia.shop.ApiClient;
import io.avalia.shop.ApiException;
import io.avalia.shop.Configuration;
import io.avalia.shop.models.*;
import io.avalia.shop.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/api-shop");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String authorization = "authorization_example"; // String | 
    String userId = "userId_example"; // String | 
    try {
      List<Order> result = apiInstance.getOrders(authorization, userId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#getOrders");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  |
 **userId** | **String**|  |

### Return type

[**List&lt;Order&gt;**](Order.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | orders retrived successfully |  -  |
**403** | forbidden |  -  |

<a name="getProduct"></a>
# **getProduct**
> Product getProduct(productId, authorization)



get product by id

### Example
```java
// Import classes:
import io.avalia.shop.ApiClient;
import io.avalia.shop.ApiException;
import io.avalia.shop.Configuration;
import io.avalia.shop.models.*;
import io.avalia.shop.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/api-shop");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    Integer productId = 56; // Integer | 
    String authorization = "authorization_example"; // String | 
    try {
      Product result = apiInstance.getProduct(productId, authorization);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#getProduct");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Integer**|  |
 **authorization** | **String**|  |

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | product retrived successfully |  -  |
**403** | forbidden |  -  |
**404** | product no found |  -  |

<a name="getProducts"></a>
# **getProducts**
> List&lt;Product&gt; getProducts(authorization, page, size)



get the list of products

### Example
```java
// Import classes:
import io.avalia.shop.ApiClient;
import io.avalia.shop.ApiException;
import io.avalia.shop.Configuration;
import io.avalia.shop.models.*;
import io.avalia.shop.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/api-shop");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String authorization = "authorization_example"; // String | 
    Integer page = 0; // Integer | 
    Integer size = 10; // Integer | 
    try {
      List<Product> result = apiInstance.getProducts(authorization, page, size);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#getProducts");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  |
 **page** | **Integer**|  | [optional] [default to 0]
 **size** | **Integer**|  | [optional] [default to 10]

### Return type

[**List&lt;Product&gt;**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | products retrieved successfully |  -  |
**400** | unvalid page size |  -  |
**403** | forbidden |  -  |

<a name="makeOrder"></a>
# **makeOrder**
> makeOrder(authorization, order)



make an order

### Example
```java
// Import classes:
import io.avalia.shop.ApiClient;
import io.avalia.shop.ApiException;
import io.avalia.shop.Configuration;
import io.avalia.shop.models.*;
import io.avalia.shop.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/api-shop");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String authorization = "authorization_example"; // String | 
    OrderDTO order = new OrderDTO(); // OrderDTO | 
    try {
      apiInstance.makeOrder(authorization, order);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#makeOrder");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  |
 **order** | [**OrderDTO**](OrderDTO.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | order added successfully |  -  |
**400** | unvalid products |  -  |
**403** | forbidden |  -  |

