

# Product

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productId** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**price** | **Integer** |  |  [optional]



