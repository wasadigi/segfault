

# ProductDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**price** | **Integer** |  |  [optional]



