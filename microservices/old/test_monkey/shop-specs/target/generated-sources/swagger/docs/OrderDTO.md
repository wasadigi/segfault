

# OrderDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ownerId** | **String** |  |  [optional]
**products** | **List&lt;Integer&gt;** |  |  [optional]



