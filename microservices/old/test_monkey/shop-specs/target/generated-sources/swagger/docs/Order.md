

# Order

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **Integer** |  |  [optional]
**ownerId** | **String** |  |  [optional]
**products** | **List&lt;Integer&gt;** |  |  [optional]



