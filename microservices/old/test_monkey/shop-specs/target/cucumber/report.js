$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("OrderCreation.feature");
formatter.feature({
  "line": 1,
  "name": "Creation of order",
  "description": "",
  "id": "creation-of-order",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 1138158854,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "successful creation of order",
  "description": "",
  "id": "creation-of-order;successful-creation-of-order",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have a valid token",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I have a order credential",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I POST it to the /shop endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I receive a 204 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_a_valid_token()"
});
formatter.result({
  "duration": 65642,
  "status": "passed"
});
formatter.match({
  "location": "orderCreation.iHaveAOrderCredential()"
});
formatter.result({
  "duration": 242507,
  "status": "passed"
});
formatter.match({
  "location": "orderCreation.iPOSTItToTheShopEndpoint()"
});
formatter.result({
  "duration": 3170180047,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "204",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 3910752,
  "error_message": "java.lang.AssertionError: expected:\u003c204\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 204 status code(OrderCreation.feature:10)\r\n",
  "status": "failed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 443442,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "unauthorized creation of order",
  "description": "",
  "id": "creation-of-order;unauthorized-creation-of-order",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I have an invalid token",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I have a order credential",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "I POST it to the /shop endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I receive a 403 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_an_invalid_token()"
});
formatter.result({
  "duration": 29903,
  "status": "passed"
});
formatter.match({
  "location": "orderCreation.iHaveAOrderCredential()"
});
formatter.result({
  "duration": 91897,
  "status": "passed"
});
formatter.match({
  "location": "orderCreation.iPOSTItToTheShopEndpoint()"
});
formatter.result({
  "duration": 3051923576,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 526222,
  "error_message": "java.lang.AssertionError: expected:\u003c403\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 403 status code(OrderCreation.feature:16)\r\n",
  "status": "failed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 1086360,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "invalid creation of order",
  "description": "",
  "id": "creation-of-order;invalid-creation-of-order",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 19,
  "name": "I have a valid token",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "I have a invalid order credential",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "I POST it to the /shop endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "I receive a 400 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_a_valid_token()"
});
formatter.result({
  "duration": 36832,
  "status": "passed"
});
formatter.match({
  "location": "orderCreation.iHaveAInvalidOrderCredential()"
});
formatter.result({
  "duration": 153162,
  "status": "passed"
});
formatter.match({
  "location": "orderCreation.iPOSTItToTheShopEndpoint()"
});
formatter.result({
  "duration": 3051029398,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 244695,
  "error_message": "java.lang.AssertionError: expected:\u003c400\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 400 status code(OrderCreation.feature:22)\r\n",
  "status": "failed"
});
formatter.uri("OrderRetrieval.feature");
formatter.feature({
  "line": 1,
  "name": "Retrieval of order",
  "description": "",
  "id": "retrieval-of-order",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 518930,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "retrieval of order",
  "description": "",
  "id": "retrieval-of-order;retrieval-of-order",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have a valid token",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I GET it to the /shop endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I receive a 200 status code",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I receive an array list",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_a_valid_token()"
});
formatter.result({
  "duration": 22974,
  "status": "passed"
});
formatter.match({
  "location": "orderRetrieval.iGETItToTheShopEndpoint()"
});
formatter.result({
  "duration": 3060746471,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 218439,
  "error_message": "java.lang.AssertionError: expected:\u003c200\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 200 status code(OrderRetrieval.feature:9)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "productRetrieval.i_receive_an_ArrayList()"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 617756,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "unauthorized retrieval of order",
  "description": "",
  "id": "retrieval-of-order;unauthorized-retrieval-of-order",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I have an invalid token",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I GET it to the /shop endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "I receive a 403 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_an_invalid_token()"
});
formatter.result({
  "duration": 32821,
  "status": "passed"
});
formatter.match({
  "location": "orderRetrieval.iGETItToTheShopEndpoint()"
});
formatter.result({
  "duration": 3062376923,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 226462,
  "error_message": "java.lang.AssertionError: expected:\u003c403\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 403 status code(OrderRetrieval.feature:15)\r\n",
  "status": "failed"
});
formatter.uri("productCreation.feature");
formatter.feature({
  "line": 1,
  "name": "Creation of products",
  "description": "",
  "id": "creation-of-products",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 530599,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "successful creation of products",
  "description": "",
  "id": "creation-of-products;successful-creation-of-products",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have a valid admin token",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I have a product credential",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I POST it to the /products",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I receive a 201 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productCreation.i_have_a_valid_adminToken()"
});
formatter.result({
  "duration": 147693,
  "status": "passed"
});
formatter.match({
  "location": "productCreation.i_have_a_product_credential()"
});
formatter.result({
  "duration": 130188,
  "status": "passed"
});
formatter.match({
  "location": "productCreation.i_POST_it_to_the_products()"
});
formatter.result({
  "duration": 3068307962,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "201",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 262565,
  "error_message": "java.lang.AssertionError: expected:\u003c201\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 201 status code(productCreation.feature:10)\r\n",
  "status": "failed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 455841,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "unauthorized creation of products cause of invalid token",
  "description": "",
  "id": "creation-of-products;unauthorized-creation-of-products-cause-of-invalid-token",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I have an invalid token",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I have a product credential",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "I POST it to the /products",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I receive a 403 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_an_invalid_token()"
});
formatter.result({
  "duration": 33185,
  "status": "passed"
});
formatter.match({
  "location": "productCreation.i_have_a_product_credential()"
});
formatter.result({
  "duration": 94815,
  "status": "passed"
});
formatter.match({
  "location": "productCreation.i_POST_it_to_the_products()"
});
formatter.result({
  "duration": 3063850930,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 211875,
  "error_message": "java.lang.AssertionError: expected:\u003c403\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 403 status code(productCreation.feature:16)\r\n",
  "status": "failed"
});
formatter.uri("productDeletion.feature");
formatter.feature({
  "line": 1,
  "name": "deletion of products",
  "description": "",
  "id": "deletion-of-products",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 459852,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "successful deletion of product",
  "description": "",
  "id": "deletion-of-products;successful-deletion-of-product",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have a valid token",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I create a product and POST it to /products",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I DELETE it to the /products endpoint with 2 id",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I receive a 204 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_a_valid_token()"
});
formatter.result({
  "duration": 23704,
  "status": "passed"
});
formatter.match({
  "location": "productDeletion.i_create_a_product_and_POST_it_to_products()"
});
formatter.result({
  "duration": 3064499319,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertFalse(Assert.java:64)\r\n\tat org.junit.Assert.assertFalse(Assert.java:74)\r\n\tat io.avalia.shop.api.spec.steps.productDeletion.i_create_a_product_and_POST_it_to_products(productDeletion.java:39)\r\n\tat ✽.Given I create a product and POST it to /products(productDeletion.feature:8)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 43
    }
  ],
  "location": "productDeletion.iDELETEItToTheProductsEndpointWithId(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "204",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 1401073,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "deletion of not found product",
  "description": "",
  "id": "deletion-of-products;deletion-of-not-found-product",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I have a valid token",
  "keyword": "Given "
});
formatter.step({
  "comments": [
    {
      "line": 14,
      "value": "## invalid id"
    }
  ],
  "line": 15,
  "name": "I DELETE it to the /products endpoint with 0 id",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I receive a 404 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_a_valid_token()"
});
formatter.result({
  "duration": 26621,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 43
    }
  ],
  "location": "productDeletion.iDELETEItToTheProductsEndpointWithId(int)"
});
formatter.result({
  "duration": 3065623970,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "404",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 227920,
  "error_message": "java.lang.AssertionError: expected:\u003c404\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 404 status code(productDeletion.feature:16)\r\n",
  "status": "failed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 457664,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "unauthorized deletion of product",
  "description": "",
  "id": "deletion-of-products;unauthorized-deletion-of-product",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 19,
  "name": "I have an invalid token",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "I DELETE it to the /products endpoint with 2 id",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "I receive a 403 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_an_invalid_token()"
});
formatter.result({
  "duration": 54701,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 43
    }
  ],
  "location": "productDeletion.iDELETEItToTheProductsEndpointWithId(int)"
});
formatter.result({
  "duration": 3065507274,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 220628,
  "error_message": "java.lang.AssertionError: expected:\u003c403\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 403 status code(productDeletion.feature:21)\r\n",
  "status": "failed"
});
formatter.uri("productRetrieval.feature");
formatter.feature({
  "line": 1,
  "name": "Retrieval of products",
  "description": "",
  "id": "retrieval-of-products",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 440889,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "successful retrieval of products",
  "description": "",
  "id": "retrieval-of-products;successful-retrieval-of-products",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have a valid token",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I GET it to the /products endpoint with 2 pages and 3 size",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I receive a 200 status code",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I receive an array list",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_a_valid_token()"
});
formatter.result({
  "duration": 24798,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 40
    },
    {
      "val": "3",
      "offset": 52
    }
  ],
  "location": "productRetrieval.i_GET_it_to_the_products_endpoint_with_pages_and_size(int,int)"
});
formatter.result({
  "duration": 3075682011,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 227556,
  "error_message": "java.lang.AssertionError: expected:\u003c200\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 200 status code(productRetrieval.feature:9)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "productRetrieval.i_receive_an_ArrayList()"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 559773,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "unauthorized retrieval of products cause of invalid pages",
  "description": "",
  "id": "retrieval-of-products;unauthorized-retrieval-of-products-cause-of-invalid-pages",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I have a valid token",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I GET it to the /products endpoint with negative 2 pages and 3 size",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "I receive a 400 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_a_valid_token()"
});
formatter.result({
  "duration": 27716,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 49
    },
    {
      "val": "3",
      "offset": 61
    }
  ],
  "location": "productRetrieval.i_GET_it_to_the_products_endpoint_with_negative_Pages_And_Size(int,int)"
});
formatter.result({
  "duration": 3060328191,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 331123,
  "error_message": "java.lang.AssertionError: expected:\u003c400\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 400 status code(productRetrieval.feature:15)\r\n",
  "status": "failed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 690325,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "unauthorized retrieval of products cause of invalid  size",
  "description": "",
  "id": "retrieval-of-products;unauthorized-retrieval-of-products-cause-of-invalid--size",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "I have a valid token",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "I GET it to the /products endpoint with 2 pages and 0 size",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I receive a 400 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_a_valid_token()"
});
formatter.result({
  "duration": 28444,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 40
    },
    {
      "val": "0",
      "offset": 52
    }
  ],
  "location": "productRetrieval.i_GET_it_to_the_products_endpoint_with_pages_and_size(int,int)"
});
formatter.result({
  "duration": 3066139982,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 233391,
  "error_message": "java.lang.AssertionError: expected:\u003c400\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 400 status code(productRetrieval.feature:20)\r\n",
  "status": "failed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Shop server",
  "keyword": "Given "
});
formatter.match({
  "location": "productRetrieval.there_is_a_shop_server()"
});
formatter.result({
  "duration": 456570,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "unauthorized retrieval of products cause of invalid token",
  "description": "",
  "id": "retrieval-of-products;unauthorized-retrieval-of-products-cause-of-invalid-token",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 23,
  "name": "I have an invalid token",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I GET it to the /products endpoint with 2 pages and 3 size",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I receive a 403 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "productRetrieval.i_have_an_invalid_token()"
});
formatter.result({
  "duration": 22974,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 40
    },
    {
      "val": "3",
      "offset": 52
    }
  ],
  "location": "productRetrieval.i_GET_it_to_the_products_endpoint_with_pages_and_size(int,int)"
});
formatter.result({
  "duration": 3066585612,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 12
    }
  ],
  "location": "productRetrieval.i_receive_a_statusCode(int)"
});
formatter.result({
  "duration": 297573,
  "error_message": "java.lang.AssertionError: expected:\u003c403\u003e but was:\u003c502\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat io.avalia.shop.api.spec.steps.productRetrieval.i_receive_a_statusCode(productRetrieval.java:59)\r\n\tat ✽.Then I receive a 403 status code(productRetrieval.feature:25)\r\n",
  "status": "failed"
});
});