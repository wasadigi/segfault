/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech) (4.2.1).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
package io.avalia.shop.api;

import io.avalia.shop.api.model.Order;
import io.avalia.shop.api.model.OrderDTO;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-01-19T21:59:49.029+01:00[Europe/Berlin]")

@Validated
@Api(value = "shop", description = "the shop API")
public interface ShopApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "", nickname = "deleteOrder", notes = "delete an order", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "order deleted successfully"),
        @ApiResponse(code = 403, message = "forbidden"),
        @ApiResponse(code = 404, message = "order not found") })
    @RequestMapping(value = "/shop",
        method = RequestMethod.DELETE)
    default ResponseEntity<Void> deleteOrder(@ApiParam(value = "" ,required=true) @RequestHeader(value="Authorization", required=true) String authorization,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "orderId", required = true) Integer orderId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    @ApiOperation(value = "", nickname = "getOrders", notes = "get the user's orders", response = Order.class, responseContainer = "List", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "orders retrived successfully", response = Order.class, responseContainer = "List"),
        @ApiResponse(code = 403, message = "forbidden") })
    @RequestMapping(value = "/shop",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    default ResponseEntity<List<Order>> getOrders(@ApiParam(value = "" ,required=true) @RequestHeader(value="Authorization", required=true) String authorization,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "userId", required = true) String userId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"orderId\" : 0, \"ownerId\" : \"ownerId\", \"products\" : [ 6, 6 ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    @ApiOperation(value = "", nickname = "makeOrder", notes = "make an order", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "order added successfully"),
        @ApiResponse(code = 400, message = "unvalid products"),
        @ApiResponse(code = 403, message = "forbidden") })
    @RequestMapping(value = "/shop",
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<Void> makeOrder(@ApiParam(value = "" ,required=true) @RequestHeader(value="Authorization", required=true) String authorization,@ApiParam(value = "" ,required=true )  @Valid @RequestBody OrderDTO order) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
