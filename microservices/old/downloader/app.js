const express = require("express");
const app = express();
const fetch = require("node-fetch");
const axios = require("axios");

app.get("/hello", function(req, res) {
  res.send("Hello World!");
});

app.get("/user/signin/callback", async function(req, res) {
  //attention use state pour sécurisé tout ça https://developer.github.com/apps/building-oauth-apps/authorizing-oauth-apps/

  let TemporaryCode = req.query.code; //this give acces to the user
  if (!TemporaryCode) {
    return res.send({
      succes: false,
      message: "error no code"
    });
  } else {
    let token;
    // TODO :: POST request to
    await fetch(
      "https://github.com/login/oauth/access_token" +
        `?client_id=bc3f09395c1c29ad272b&client_secret=7175938f15f901426cb625382871713a493b97fd&code=${TemporaryCode}`,
      {
        method: "POST"
      }
    )
      .then(data => data.text())
      .then(response => {
        token = response.split("=")[1].split("&")[0];
      })
      .catch(err => console.log(err));
    if (token != undefined) {
      axios
        .get("https://api.github.com/user", {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `token ${token}`
          }
        })
        .then(response => console.log(response.data))
        .catch(err => console.log(err));
    }
    res.send(token);
  }
});

app.listen(3000, function() {
  console.log("Example app listening on port 3000!");
});
