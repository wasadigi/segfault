var express = require('express');
var router = express.Router();
const fetch = require('node-fetch');

// middleware that is specific to this router
router.use(async function timeLog(req, res, next) {
  console.log('Time: ', Date.now());
  let TemporaryCode = req.query.code; //this give acces to the user 
    if (!TemporaryCode) {
    return res.send({
      succes: false,
      message: "error no code"
    });
  } else {
    // TODO :: POST request to
     await fetch("https://github.com/login/oauth/access_token",{
        method : "GET",
        headers : {
            "client_secret":"7175938f15f901426cb625382871713a493b97fd",
            "code" : TemporaryCode
        }
    })
    .then(Response => Response.json()).then(data => console.log(data))
    .catch(err => console.log(err))
}
  next();
});

router.get("/", async function(req, res) {
    
});

module.exports = router;
