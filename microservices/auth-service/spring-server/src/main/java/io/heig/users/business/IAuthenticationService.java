package io.heig.users.business;

public interface IAuthenticationService {
    String hashPassword(String plainTextPassword);
    boolean checkPassword(String plainTextPassword, String hashedPassword);
}
