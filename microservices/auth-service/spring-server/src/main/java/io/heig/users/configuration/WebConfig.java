package io.heig.users.configuration;

import io.heig.users.api.interceptor.SecurityInterceptor;
import io.heig.users.api.interceptor.SecurityInterceptorExceptOnGet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Bean
    SecurityInterceptor securityInterceptor() {
        return new SecurityInterceptor();
    }
    @Bean
    SecurityInterceptorExceptOnGet securityInterceptorExceptOnGet() {
        return new SecurityInterceptorExceptOnGet();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(securityInterceptorExceptOnGet()).addPathPatterns("/users/**");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //todo : * accepted
        registry.addMapping("/**");
    }
}
