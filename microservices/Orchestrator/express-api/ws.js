var WebSocketServer = require("ws").Server,
  wss = new WebSocketServer({ port: 40510 });

wss.on("connection", function (ws) {
  ws.on("message", function (message) {
    let pipelineID = message.split(/\|/g);
    var listener = function (remaningTask, user) {
      if (ws.readyState == 1) {
        ws.send(remaningTask);
        if (remaningTask == 0) {
          ws.close();
          delete this;
        }
      }
    };
    usersPipelines.registerProgressListener(
      usersPipelines.getPipeline(
        pipelineID[0],
        pipelineID[1],
        pipelineID[2],
        pipelineID[3]
      ),
      listener
    );
  });
});
