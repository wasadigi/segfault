var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'orchestrator API',
    //todo : make link to website
    message: `please connect to our service on this website and use /orchestrator for using our services`
  });
});

module.exports = router;