/**
 * this route only respond tha server is alive
 */
const router = require('express').Router();

router.get('/', async (req, res) => {
  res.status(200).send('Server is alive');
});

module.exports = router;
