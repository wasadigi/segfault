const router = require("express").Router();
const httpStatus = require("http-status");
const { AlreadyExistError } = require("../models/error");

/**
 * create a description pass in the request for this user
 */
router.post("/description", async (req, res) => {
  try {
    let creationDate = await usersDescriptions.addDescription(
      req.body,
      req.user
    );
    res.status(httpStatus.CREATED).send(creationDate);
  } catch (err) {
    if (err instanceof AlreadyExistError) {
      res.status(httpStatus.CONFLICT).send(err);
    } else if (err instanceof ReferenceError) {
      res.status(httpStatus.FAILED_DEPENDENCY).send(err);
    } else {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).send(err);
    }
  }
});
/**
 * return all descriptions for this user
 */
router.get("/descriptions", async (req, res) => {
  try {
    let descriptionsMap = await usersDescriptions.getAll(req.user);
    res.status(httpStatus.OK).send(descriptionsMap);
  } catch (err) {
    if (err instanceof AlreadyExistError) {
      res.status(httpStatus.CONFLICT).send(err);
    } else if (err instanceof ReferenceError) {
      res.status(httpStatus.FAILED_DEPENDENCY).send(err);
    } else {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).send(err);
    }
  }
});

router.get("/description/:description", (req, res) => {
  try {
    let name = JSON.parse(req.params.description).name;
    let version = JSON.parse(req.params.description).version;
    let description = usersDescriptions.get(name, version, req.user);
    res.status(httpStatus.OK).send(description);
  } catch (err) {
    throw err;
  }
});


router.get("", async (req, res) => {
  try {
    let pipelinesMap = await usersPipelines.getAll(req.user);
    res.status(httpStatus.OK).send(pipelinesMap);
  } catch (err) {
    if (err instanceof AlreadyExistError) {
      res.status(httpStatus.CONFLICT).send(err);
    } else if (err instanceof ReferenceError) {
      res.status(httpStatus.FAILED_DEPENDENCY).send(err);
    } else {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).send(err);
    }
  }
});


router.post("", async (req, res) => {
  try {
    let pipeline = await usersPipelines.addPipeline(req.user, req.body);
    usersPipelines.run(pipeline, req.user);
    res.status(httpStatus.OK).send(pipeline);
  } catch (error) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).send(error);
  }
});

router.get("/results/:id", async (req, res) => {
  try {
    let id = req.params.id.split("-");
    let artifacts = await usersPipelines.getResults(
      req.user,
      id[0],
      id[1],
      id[2]
    );
    res.status(httpStatus.OK).send(JSON.stringify(artifacts));
  } catch (e) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).send(e);
    throw e;
  }
});


module.exports = router;
