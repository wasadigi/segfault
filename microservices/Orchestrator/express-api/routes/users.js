/**
 * routes and middlewares that users have access
 */

var express = require("express");
var router = express.Router();

const { checkJWT } = require("./middleware/auth.middleware");
const alive = require("./alive.route");
const pipeline = require("./pipeline.route");

/**this route only respond "I'm alive" */
router.use("/alive", alive); //he don't need to be connect

/** user has to connect for all others routes*/
router.get("/*", checkJWT);
router.post("/*", checkJWT);

/** pipeline */
router.use("/pipeline", pipeline);

module.exports = router;
