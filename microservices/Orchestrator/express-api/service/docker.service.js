var Docker = require("dockerode");

function streamToString(stream) {
  const chunks = [];
  return new Promise((resolve, reject) => {
    stream.on("data", (chunk) => chunks.push(chunk));
    stream.on("error", reject);
    stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
  });
}

class DockerService {
  constructor() {
    this.docker = new Docker({
      socketPath: "/var/run/docker.sock"
    });
  }
  async createAndRunImage(task) {
    //permet de résoudre les multiples artifacts
    var auxContainer;
    await this.docker
      .createContainer({
        Image: task.image,
        Cmd: task.command,
        Volumes: {
          "/repositoriesVolume": {},
          "/resultsVolume": {},
          "/tempsVolume": {},
          "/descriptionsVolume": {}
        },
        HostConfig: {
          Binds: [
            "repositoriesHostVolume:/repositoriesVolume:rw",
            "resultsHostVolume:/resultsVolume:rw",
            "tempsHostVolume:/tempsVolume:rw",
            "descriptionsHostVolume:/descriptionsVolume:rw"
          ]
        },
        Tty: true
      })
      .then(async function (container) {
        auxContainer = container;
        container.attach(
          { stream: true, stdout: true, stderr: true },
          async function (err, stream) {
            var resultPromise = await streamToString(stream);
            let result = await resultPromise;
            task.result = result;
          }
        );
        await auxContainer.start();
        //Block until a container stops, then returns the exit code.
        let waiting = await auxContainer.wait();
        return;
      })
      .then(function (data) {
        return auxContainer.remove();
      })
      .then(function (data) {
        //console.log(`container of ${JSON.stringify(task)} removed`);
      })
      .catch(function (err) {
        console.log(
          "erreur lors de l'execution de la tash " + JSON.stringify(task)
        );
        console.log(err);
      });
  }
}

module.exports = {
  DockerService
};
