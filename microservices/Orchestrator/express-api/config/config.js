const { env } = process;

const config = {
  //default_secret_do_not_use_this_outside_testing_purposes
  JWT_SECRET: env.token_secret || 'jwt_secret',
};

module.exports = { config };
