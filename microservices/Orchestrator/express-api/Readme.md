# utils 

- see the content of volume 

docker run -v repositoriesHostVolume:/repositoriesVolume -v resultsHostVolume:/resultsVolume -v tempsHostVolume:/tempsVolume -v descriptionsHostVolume:/descriptionsVolume  -v pipelinesHostVolume:/pipelinesVolume -it readwrite /bin/sh

- remove all content 

cd repositoriesVolume && rm -R * && cd .. && cd resultsVolume && rm -R * && cd .. && cd descriptionsVolume && rm -R * && cd .. cd pipelinesVolume && rm -R *

