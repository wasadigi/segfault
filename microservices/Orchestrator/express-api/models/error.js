/**
 * error if we need to create something but she's already exist
 */

class AlreadyExistError extends Error {
  constructor(message) {
    super(message);
    this.name = "Already Exist";
  }
}

module.exports = { AlreadyExistError };
